package main

import (
	"testing"
	"strings"
)

func TestOctToHex(t *testing.T) {
	ans := toBaseX("13324557", 8, 16)
	if strings.ToUpper(ans) != "2DA96F" {
		t.Errorf("toBaseX(\"13324557\", 8, 16) = %s; want 2DA96F", ans)
	}
}

func TestComplex(t *testing.T) {
	ans := toBaseX("J", 20, 17)
	if strings.ToUpper(ans) != "12" {
		t.Errorf("toBaseX(\"J\", 20, 17) = %s; want 12", ans)
	}
}

func TestComplex2(t *testing.T) {
	ans := toBaseX("JJ", 20, 17)
	if strings.ToUpper(ans) != "168" {
		t.Errorf("toBaseX(\"JJ\", 20, 17) = %s; want 168", ans)
	}
}