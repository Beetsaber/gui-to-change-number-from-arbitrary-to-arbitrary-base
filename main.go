package main

import (
	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"
	big "math/big"
	b64 "encoding/base64"
	"strconv"
)

// build GUI without cmd windows:
// make sure that "rsrc.syso" and "main.exe.manifest" are next to this file when building
//go build -ldflags="-H windowsgui"

// docs: https://godoc.org/github.com/lxn/walk

var inTE, outBin, outOct, outHEX, outCUSTOM, outBase64Enc, outBase64Dec *walk.TextEdit
var inBaseDropdown, inCustomBaseDropdown *walk.ComboBox
func main() {

	var possibleBases []string = make([]string, big.MaxBase-minBase()+1)
	for i := minBase(); i <= big.MaxBase; i++ {
		possibleBases[i-minBase()] = strconv.Itoa(i)
	}

	MainWindow{
		Title:   "Change input from any base to any other base",
		Size:    Size{1000, 500},
		MinSize: Size{600, 400},
		Layout:  VBox{},
		Children: []Widget{
			Label{
				Text: "input -> different base and decode/encode base 64",
			},
			HSplitter{
				Children: []Widget{
					Composite{
						Layout: VBox{},
						Children: []Widget{
							Label{Text: "Input text:",},
							ComboBox{AssignTo: &inBaseDropdown, Model: possibleBases, CurrentIndex: 10-minBase(), OnCurrentIndexChanged: populateFields, ToolTipText: "Enter base of number"},
							TextEdit{AssignTo: &inTE, OnKeyUp: callbackFunction, VScroll: true, ToolTipText: "Enter number or text for b64 encode/decode"},
							PushButton{Text: "paste from clipboard", OnClicked: pasteClipboard},
						},
					},
					Composite{
						Layout: VBox{},
						Children: []Widget{
							Label{Text: "Binary:",},
							TextEdit{AssignTo: &outBin, ReadOnly: true},
						},
					},
					Composite{
						Layout: VBox{},
						Children: []Widget{
							Label{Text: "Octal:",},
							TextEdit{AssignTo: &outOct, ReadOnly: true},
						},
					},
					Composite{
						Layout: VBox{},
						Children: []Widget{
							Label{Text: "Hexadecimal:",},
							TextEdit{AssignTo: &outHEX, ReadOnly: true},
						},
					},
					Composite{
						Layout: VBox{},
						Children: []Widget{
							Label{Text: "Custom base:",},
							ComboBox{AssignTo: &inCustomBaseDropdown, Model: possibleBases, OnCurrentIndexChanged: populateFields},
							TextEdit{AssignTo: &outCUSTOM, ReadOnly: true, ToolTipText: "The result uses the lower-case letters 'a' to 'z' for digit values 10 to 35, and the upper-case letters 'A' to 'Z' for digit values 36 to 61."},
						},
					},
					Composite{
						Layout: VBox{},
						Children: []Widget{
							Label{Text: "Base 64 encoded:",},
							TextEdit{AssignTo: &outBase64Enc, ReadOnly: true, VScroll: true},
						},
					},
					Composite{
						Layout: VBox{},
						Children: []Widget{
							Label{Text: "Base 64 decoded:",},
							TextEdit{AssignTo: &outBase64Dec, ReadOnly: true, VScroll: true},
						},
					},
				},
			},
			PushButton{
				Text: "EXECUTE",
				OnClicked: populateFields,
			},
		},
	}.Run()

}

// this function is called OnKeyUp of first field(inTE) and it does the same as clicking EXECUTE button
func callbackFunction(key walk.Key) {
	populateFields()
}

func pasteClipboard() {
	x, err := walk.Clipboard().Text()
	if err != nil {
		return
	}
	inTE.SetText(x)
	populateFields()
}

func populateFields() {

	// get text from first field
	strVal := inTE.Text()

	strValBase := 10// default base of strVal
	inputDropdownSelectionInt, err := strconv.Atoi(inBaseDropdown.Text())
	if err == nil {
		strValBase = inputDropdownSelectionInt
	}

	outBin.SetText(toBaseX(strVal, strValBase, 2))
	outOct.SetText(toBaseX(strVal, strValBase, 8))
	outHEX.SetText(toBaseX(strVal, strValBase, 16))

	customDropdownSelectionInt, err := strconv.Atoi(inCustomBaseDropdown.Text())
	if err == nil {
		outCUSTOM.SetText(toBaseX(strVal, strValBase, customDropdownSelectionInt))
	}


	encodedAsBase64 := b64.StdEncoding.EncodeToString([]byte(strVal))
	outBase64Enc.SetText(encodedAsBase64)

	// make sure strVal is of size which is multiple of 4 by adding extra '=' signs to the end
	if len(strVal) % 4 != 0 {
		for i := 0; i < (len(strVal) % 4); i++ {
			strVal += "="
		}
	}
	decodedFromBase64, err := b64.StdEncoding.DecodeString(strVal)
	
	if err != nil {
		outBase64Dec.SetText("Error: b64.StdEncoding.DecodeString\n" + err.Error())
	} else {
		if len(decodedFromBase64) == 0 { return }// prevent out of range crashing from decodedFromBase64[0]
		if decodedFromBase64[0] == byte("\x00"[0]) {
			outBase64Dec.SetText("Error: string with NUL passed to StringToUTF16")
			return
		}
		printableDecodedFromBase64 := ""
		for _, c := range(decodedFromBase64) {
			if c < 32 || c > 127 {
				printableDecodedFromBase64 += "?"
			} else {
				printableDecodedFromBase64 += string(c)
			}
		}
		outBase64Dec.SetText(printableDecodedFromBase64)
	}

}

// convert numString considered in numBase into baseX
// ex. toBaseX("11", 8, 16) ie. 11(8) -> 9(16)
func toBaseX(numString string, numBase int, baseX int) string {
	if numBase < minBase() || numBase > big.MaxBase {
		return "Maximum allowed base to convert from is " + strconv.Itoa(big.MaxBase)
	}
	if baseX < minBase() || baseX > big.MaxBase {
		return "Maximum allowed base to convert to is " + strconv.Itoa(big.MaxBase)
	}

	n := new(big.Int)
	n, ok := n.SetString(numString, numBase)

	if !ok {
		return "Error: input value is not valid base (" + strconv.Itoa(numBase) + ") value"
	}

	return n.Text(baseX)// convert to string in baseX
}

func minBase() int {
	return 2
}